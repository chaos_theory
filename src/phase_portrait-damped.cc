#include <iostream>
#include <stdio.h>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <math.h>

using namespace std;
#define pi 3.14159265

double Function(double t, double x, double v)
{
	// intial conditions
	/*double omega = 2;
	double c = 0.1;	// damping constant
	double F = 0.5;  // driving force
	*/
	/*double omega = 1.4;
	double c = 0.1;
	double F = 0.1;
	*/
	double omega = 1;
	double c = 0.5;
	double F = 0.3;

	// duffing oscillator equation
	return -c*v + x - pow(x,3) + F*cos(omega*t); 
}

int main(){
	// initial conditions
	/*double x = -3*pi; // in radians
	double v = -1;	// in ms^-1
	*/
	double x = -3*pi;
	double v = -0.1;
	
	double t = 0;	// in t
	double h = 0.05;	// step size
	double n = 0;	// step number
	double tmax = 1000;	// time range

	// misc
	int cache = 0;

	// runge-kutter variables
	double k_1 = 0;
	double k_2 = 0;
	double k_3 = 0;
	double k_4 = 0;

	n = tmax/h;	// calculate number of steps
	ofstream datafile;
	datafile.open("../data/phase_portrait-damped.data");
	datafile << "#t\tx\tv\n"; 
	
	cache = v; // see NOTE below 
	cout << "Simulation is running!" << endl;
	for(double k = x; k <= 3*pi; k += pi){
		// reset intial conditions
		v = cache; 
		// NOTE: after one run of x the v would be different therefore
		// you need to reset it again by having a sort of cache on
		// the outside of this for loop, or else it would use previous
		// results and mess up the loop.

		for(double i = v; i <= 4.5; i += 0.5){
			// reset initial conditions
			t=0; 
			x=k; // similar to NOTE above, but this time with x
			v=i;

			// runge-kutta approximations
			for(int j = 0; j <= n; j++){	
				datafile << fixed;
				datafile << setprecision(2) << t << "\t"; 
				datafile << setprecision(4) << x << "\t";
				datafile << setprecision(4) << v << endl;
				
				k_1 = h * Function(t, x, v);
				k_2 = h * Function(t + h/2, x + k_1*h/2, v);
				k_3 = h * Function(t + h/2, x + k_2*h/2, v);
				k_4 = h * Function(t + h, x + k_3*h, v);
					
				// update theta and velocity
				v += (k_1 + 2*k_2 + 2*k_3 + k_4)*h/6;	
				k_1 = k_2 = k_3 = k_4 = h*v;
				x += (k_1 + 2*k_2 + 2*k_3 + k_4)*h/6;	
				t += h;
			}	
			datafile << endl; // this is to separate different result sets
		}
	}
	cout << "All simulations complete!" << endl;
	datafile.close();
	return 0;
}
