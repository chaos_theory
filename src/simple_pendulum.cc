#include <iostream>
#include <stdio.h>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <math.h>
using namespace std;
#define pi 3.14159265

double Function(double t, double x)
{
	// intial conditions
	double omega = 1;

	// simple pedendulum equation
	return  -pow(omega,2)*sin(x);
}

int main(){
	// initial conditions
	double x = 1; // in radians
	double h = 0.05;	// step size
	double v = 0;
	double t = 0;
	double n = 0;	// step number
	double tmax = 1000;	// time range
	
	// runge-kutter variables
	double k_1 = 0;
	double k_2 = 0;
	double k_3 = 0;
	double k_4 = 0;

	n = tmax/h;	

	ofstream datafile;
	datafile.open("../data/simple_pendulum.data");
	datafile << "#t\tx\tv\n"; 

	for(int i=0; i<n; i++){	
		datafile << fixed;
		datafile << setprecision(2) << t << "\t"; 
		datafile << setprecision(4) << x << "\t";
		datafile << setprecision(4) << v << endl;

		// runge-kutter approximations
		k_1 = h * Function(t, x);
		k_2 = h * Function(t + h/2, x + k_1*h/2);
		k_3 = h * Function(t + h/2, x + k_2*h/2);
		k_4 = h * Function(t + h, x + k_3*h);
			
		// update theta and velocity
		v = v + (k_1 + 2*k_2 + 2*k_3 + k_4)*h/6;	
		k_1 = k_2 = k_3 = k_4 = h*v;
		x +=  (k_1 + 2*k_2 + 2*k_3 + k_4)*h/6;	
		t += h;
	}	 
	datafile.close();
	return 0;
}
