#include <iostream>
#include <stdio.h>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <math.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>

using namespace std;
// constants
#define pi 3.14159265

// initial conditions
#define x_0 -3*pi
#define v_0 1
#define t_0 0
#define c_0 0.1
#define omega 2
#define	F_0 0.9 

double Function(double t, double x, double v, double F)
{
	return  -c_0*v - sin(x) - (F*cos(omega*t) * sin(x));
	//return F*cos(omega*t) - c_0*v + x - pow(x,3);
}

void changemode(int dir)
{
	static struct termios oldt, newt;

	if ( dir == 1 ){
		tcgetattr( STDIN_FILENO, &oldt);
		newt = oldt;
		newt.c_lflag &= ~( ICANON | ECHO );
		tcsetattr( STDIN_FILENO, TCSANOW, &newt);
	}
	else
		tcsetattr( STDIN_FILENO, TCSANOW, &oldt);
}

int kbhit (void)
{
	struct timeval tv;
	fd_set rdfs;

	tv.tv_sec = 0;
	tv.tv_usec = 0;

	FD_ZERO(&rdfs);
	FD_SET (STDIN_FILENO, &rdfs);

	select(STDIN_FILENO+1, &rdfs, NULL, NULL, &tv);
	return FD_ISSET(STDIN_FILENO, &rdfs);
}

int main(){
	// oscillator parameters 
	double F = F_0;  // driving force
	double x = x_0; // in radians
	double v = v_0;	// in ms^-1
	double t = t_0;	// in t
	
	// runge-kutta parameters
	double n = 25;	// step number

	// runge-kutter variables & parameters
	double k_1 = 0;
	double k_2 = 0;
	double k_3 = 0;
	double k_4 = 0;
	//double h = (2*pi)/(omega*n);	// step size
	double h = 0.01;


	// misc
	char option;
	int quit = 0;
	ofstream datafile;
	datafile.open("../data/road_to_chaos.data");


	cout << "Simulation is running!" << endl;
	cout << "Press Ctrl-C to quit" << endl;
	changemode(1);
	while(quit == 0){
		for(int i=0; i<=n; i++){
			// pipe output to STDOUT for real-time plotting
			cout << x << " " << v << endl;
			datafile << "Time t is: " << t << endl;
			datafile << "Force F is " << F << endl;

			// rk4
			k_1 = h * Function(t, x, v, F);
			k_2 = h * Function(t + h/2, x + k_1*h/2, v, F);
			k_3 = h * Function(t + h/2, x + k_2*h/2, v, F);
			k_4 = h * Function(t + h, x + k_3*h, v, F);
			
			// update 
			v += (k_1 + 2*k_2 + 2*k_3 + k_4)*h/6;	
			k_1 = k_2 = k_3 = k_4 = h*v;
			x += (k_1 + 2*k_2 + 2*k_3 + k_4)*h/6;	
			t += h;
		}
		
		// change driving force F 
		// options: (i)ncrease F, (d)ecrease F
		if(kbhit()){
			option = getchar();
			if(option == 'i'){
				F += 0.01;
			}
			else if(option == 'd'){
				F -= 0.01;
			}
			else if(option == 'q'){
				quit = 1;
			}
		}
	}	
	changemode(0);
	datafile.close();	
	return 0;
}
