#!/usr/bin/gnuplot
set title "Time vs Displacement" 
set xlabel "Time (s)"
set ylabel "x (radians)"
set grid
plot \
"../data/simple_pendulum.data", \
"../data/duffing_oscillator.data" \
using 1:2 with lines

# gnuplot recommends setting terminal before output
set terminal png         
# The output filename; to be set after setting terminal
set output "../plots/simple_duffing_xt_plot.png"  
replot

pause -1 # wait till user finishes with graph



set title "Time vs Velocity" 
set xlabel "Time (s)"
set ylabel "Velocity (radians/s)"
set grid 
plot \
"../data/simple_pendulum.data", \
"../data/duffing_oscillator.data" using 1:3 with lines

# gnuplot recommends setting terminal before output
set terminal png         
# The output filename; to be set after setting terminal
set output "../plots/simple_duffing_vt_plot.png"  
replot

pause -1 # wait till user finishes with graph
