#!/usr/bin/gnuplot
# phase portrait for simple pendulum
set title "Road To Chaos" 
set xlabel "x (radians)"
set ylabel "v (radians/s)"
#set xrange [-2:2]
#set yrange [-1:1]
set grid
set nokey # disables the legend
plot "../data/road_to_chaos.data" using 2:3 with lines

# gnuplot recommends setting terminal before output
set terminal png         
# The output filename; to be set after setting terminal
set output "../plots/road_to_chaos.png"  
replot

pause -1 # wait till user finishes with graph

