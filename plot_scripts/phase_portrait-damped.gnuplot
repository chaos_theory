#!/usr/bin/gnuplot
# phase portrait for simple pendulum
set title "Portraits in Phase Space - Damped Motion" 
set xlabel "x (radians)"
set ylabel "v (radians/s)"
set xrange [-3*pi:3*pi]
set yrange [-3:3]
set grid
set nokey # disables the legend
plot "../data/phase_portrait-damped.data" using 2:3 with lines

# gnuplot recommends setting terminal before output
set terminal png         
# The output filename; to be set after setting terminal
set output "../plots/Portrait_Phase_Space-Damped.png"  
replot

pause -1 # wait till user finishes with graph

