#!/usr/bin/gnuplot
# The pontential energy V of a dynamical system may be  defined as
# the negative of the integral of forces (other than the damping and 
# driving force).

# For a simple pendulum 
# V = - \int{F(x) dx = - \int{-sin(x)} dx
# V = -cos(x) + C
# where we have taken l/g = 1. C is a constant of integration that 
# depends only on where the zero for the potential energy is chosen.
# We chose to let V = 0 when x = pi/2, making C zero. Thus:
# V = -cos(x) is the potential energy of our simple pendulum.

# Similarly for a duffing oscillator gives:
# V = -0.5*x^2 + 0.75*x^4
# where we habe chosen to let V = 0 at x = 0.

set title "Potential Energy of a Dynamical System" 
set xlabel "x (radiains)"
set ylabel "V(x)"
set xrange [-pi:pi]
set yrange [-1.2:2]
set grid
plot -cos(x), -0.5*x**2+0.75*x**4 with lines

# gnuplot recommends setting terminal before output
set terminal png         
# The output filename; to be set after setting terminal
set output "../plots/potential_energy_of_a_dynamical_system.png"  
replot

pause -1 # wait till user finishes with graph


